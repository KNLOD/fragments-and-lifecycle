package com.example.authwebapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.Navigation

class RegistrationFragment : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view : View = inflater.inflate(R.layout.fragment_registration, container, false)
        val signUpButton : Button = view.findViewById(R.id.sign_up)

        signUpButton.setOnClickListener{
            Navigation.findNavController(it).navigate(R.id.action_registrationFragment_to_loginFragment)
        }

        return view
    }

}