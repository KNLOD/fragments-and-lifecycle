package com.example.authwebapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.fragment.app.FragmentManager
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.NavHostFragment.Companion.findNavController
import androidx.navigation.ui.NavigationUI
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.hide()

        /**
         * Initializing NavHost and NavController as well as BottomNavigationView
         */
        val navHostFragment = supportFragmentManager
            .findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        val navController = navHostFragment.navController
        val bottomNavigationView = findViewById<BottomNavigationView>(R.id.bottom_nav)

        /**
         * Setups BottomNavigationView with NavController
         * in order to navigate and change selected item
         * without implementing onSelectedItemsListeners ourselves
         */
        NavigationUI.setupWithNavController(bottomNavigationView, navController)


        /**
         * Show or hide BottomNavigation in case of changing Destination
         */
        navController.addOnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
                R.id.homeFragment -> {
                    bottomNavigationView.visibility = View.VISIBLE
                }

                R.id.webFragment -> {
                    bottomNavigationView.visibility = View.VISIBLE
                }

                else -> bottomNavigationView.visibility = View.GONE

            }
        }
    }
}